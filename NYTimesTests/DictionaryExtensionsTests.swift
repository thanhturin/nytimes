//
//  DictionaryExtensionsTests.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/4/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import XCTest

class DictionaryExtensionsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testStringFromEmptyParameters() {
        let dictionary: [String: Any] = [:]
        let httpString = dictionary.stringFromHttpParameters()
        let expectedString = ""
        XCTAssertEqual(httpString, expectedString)
    }
    
    func testStringFromStringParameters() {
        let dictionary = ["key": "test string with spacing"]
        let httpString = dictionary.stringFromHttpParameters()
        let expectedString = "key=test%20string%20with%20spacing"
        XCTAssertEqual(httpString, expectedString)
    }
    
    func testStringFromIntParameters() {
        let dictionary = ["key": 1]
        let httpString = dictionary.stringFromHttpParameters()
        let expectedString = "key=1"
        XCTAssertEqual(httpString, expectedString)
    }
}
