//
//  SuggestionSearchServiceTests.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/4/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import XCTest

class SuggestionSearchServiceTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCacheLastestSearchQueryEmpty() {
        let suggestionSearchService = SuggestionSearchServiceDefault(suggestionSearches: [])
        suggestionSearchService.cacheLatestSearchQuery("111")
        let suggestionSearches = suggestionSearchService.suggestionSearches
        let expectedResult = ["111"]
        XCTAssertEqual(suggestionSearches, expectedResult)
    }
    
    func testCacheLastestSearchQueryOneDifferentElement() {
        let suggestionSearchService = SuggestionSearchServiceDefault(suggestionSearches: ["111"])
        suggestionSearchService.cacheLatestSearchQuery("222")
        let suggestionSearches = suggestionSearchService.suggestionSearches
        let expectedResult = ["222", "111"]
        XCTAssertEqual(suggestionSearches, expectedResult)
    }
    
    func testCacheLastestSearchQuerySameElement() {
        let suggestionSearchService = SuggestionSearchServiceDefault(suggestionSearches: ["111", "222", "333"])
        suggestionSearchService.cacheLatestSearchQuery("222")
        let suggestionSearches = suggestionSearchService.suggestionSearches
        let expectedResult = ["222", "111", "333"]
        XCTAssertEqual(suggestionSearches, expectedResult)
    }
    
    func testCacheLastestSearchQueryExceeded10Elements() {
        let suggestionSearchService = SuggestionSearchServiceDefault(suggestionSearches: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"])
        suggestionSearchService.cacheLatestSearchQuery("11")
        let suggestionSearches = suggestionSearchService.suggestionSearches
        let expectedResult = ["11", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        XCTAssertEqual(suggestionSearches, expectedResult)
    }
    
    func testEmptyCache() {
        let testKey = "unittest.SuggestionSearchServiceTests.1"
        let suggestionSearches = SuggestionSearchServiceDefault.loadCacheForRecentSearch(key: testKey)
        let expectedResult: [String] = []
        XCTAssertEqual(suggestionSearches, expectedResult)
        UserDefaults.standard.set(nil, forKey: testKey)
    }
    
    func testWrongFormatCache() {
        let testKey = "unittest.SuggestionSearchServiceTests.2"
        let data = NSKeyedArchiver.archivedData(withRootObject: "Test")
        UserDefaults.standard.set(data, forKey: testKey)
        
        let suggestionSearches = SuggestionSearchServiceDefault.loadCacheForRecentSearch(key: testKey)
        let expectedResult: [String] = []
        XCTAssertEqual(suggestionSearches, expectedResult)
        UserDefaults.standard.set(nil, forKey: testKey)
    }
    
    func testGetCache() {
        let testKey = "unittest.SuggestionSearchServiceTests.3"
        let data = NSKeyedArchiver.archivedData(withRootObject: ["test1", "test2"])
        UserDefaults.standard.set(data, forKey: testKey)
        
        let suggestionSearches = SuggestionSearchServiceDefault.loadCacheForRecentSearch(key: testKey)
        let expectedResult: [String] = ["test1", "test2"]
        XCTAssertEqual(suggestionSearches, expectedResult)
        UserDefaults.standard.set(nil, forKey: testKey)
    }
}
