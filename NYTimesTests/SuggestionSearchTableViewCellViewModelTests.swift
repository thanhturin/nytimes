//
//  SuggestionSearchTableViewCellViewModelTests.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import XCTest

class SuggestionSearchTableViewCellViewModelTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInit() {
        let vm = SuggestionSearchTableViewCellViewModel(searchQuery: "test query")
        XCTAssertEqual(vm.searchQuery, "test query")
    }
    
}
