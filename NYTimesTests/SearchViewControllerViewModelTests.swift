//
//  SearchViewControllerViewModelTests.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/5/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import XCTest

class SearchViewControllerViewModelTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSearchDidSearch() {
        let suggestionSearchService = SuggestionSearchServiceDefault(suggestionSearches: ["1", "2"])
        let vm = SearchViewControllerViewModel(delegate: nil, suggestionSearchService: suggestionSearchService)
        vm.searchDidSearch(query: "3")
        let suggestionSearches = suggestionSearchService.suggestionSearches
        let expectedResult = ["3", "1", "2"]
        XCTAssertEqual(suggestionSearches, expectedResult)
    }
}
