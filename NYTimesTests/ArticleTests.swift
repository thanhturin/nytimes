//
//  ArticleTests.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/4/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import XCTest

class ArticleTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testArticleSerializeNoDictionary() {
        let json = "None Dictionary Test"
        let article = Article.serialize(json as AnyObject)
        XCTAssertNil(article)
    }
    
    func testArticleSerializeNormal() {
        let json: [String: Any] = [
            "headline": [
                "print_headline": "Singapore’s Media Values"
            ]
            , "snippet": "The Singaporean ambassador writes that his country’s approach to content is a reflection of its values........"
            , "web_url": "https://www.nytimes.com/2017/05/26/opinion/singapores-media-values.html"
            , "pub_date": "2017-05-26T17:51:43+0000"
            , "multimedia": [
                ["subtype" : "wide", "url":"images/2017/05/22/opinion/22jawalSub/22jawalSub-thumbWide-v2.jpg"]
            ]
        ]
        let article = Article.serialize(json as AnyObject)
        XCTAssertNotNil(article)
    }
    
    func testArticleSerializeNoHeadline() {
        let json: [String: Any] = [
            "snippet": "The Singaporean ambassador writes that his country’s approach to content is a reflection of its values........"
            , "web_url": "https://www.nytimes.com/2017/05/26/opinion/singapores-media-values.html"
            , "pub_date": "2017-05-26T17:51:43+0000"
            , "multimedia": [
                ["subtype" : "wide", "url":"images/2017/05/22/opinion/22jawalSub/22jawalSub-thumbWide-v2.jpg"]
            ]
        ]
        let article = Article.serialize(json as AnyObject)
        XCTAssertNil(article)
    }
    
    func testArticleSerializeNoTitle() {
        let json: [String: Any] = [
            "headline": []
            , "snippet": "The Singaporean ambassador writes that his country’s approach to content is a reflection of its values........"
            , "web_url": "https://www.nytimes.com/2017/05/26/opinion/singapores-media-values.html"
            , "pub_date": "2017-05-26T17:51:43+0000"
            , "multimedia": [
                ["subtype" : "wide", "url":"images/2017/05/22/opinion/22jawalSub/22jawalSub-thumbWide-v2.jpg"]
            ]
        ]
        let article = Article.serialize(json as AnyObject)
        XCTAssertNil(article)
    }
    
    func testArticleSerializeNoSnippet() {
        let json: [String: Any] = [
            "headline": [
                "print_headline": "Singapore’s Media Values"
            ]
            , "web_url": "https://www.nytimes.com/2017/05/26/opinion/singapores-media-values.html"
            , "pub_date": "2017-05-26T17:51:43+0000"
            , "multimedia": [
                ["subtype" : "wide", "url":"images/2017/05/22/opinion/22jawalSub/22jawalSub-thumbWide-v2.jpg"]
            ]
        ]
        let article = Article.serialize(json as AnyObject)
        XCTAssertNil(article)
    }
    
    func testArticleSerializeNoWebURL() {
        let json: [String: Any] = [
            "headline": [
                "print_headline": "Singapore’s Media Values"
            ]
            , "snippet": "The Singaporean ambassador writes that his country’s approach to content is a reflection of its values........"
            , "pub_date": "2017-05-26T17:51:43+0000"
            , "multimedia": [
                ["subtype" : "wide", "url":"images/2017/05/22/opinion/22jawalSub/22jawalSub-thumbWide-v2.jpg"]
            ]
        ]
        let article = Article.serialize(json as AnyObject)
        XCTAssertNil(article)
    }
    
    func testArticleSerializeWrongWebURL() {
        let json: [String: Any] = [
            "headline": [
                "print_headline": "Singapore’s Media Values"
            ]
            , "snippet": "The Singaporean ambassador writes that his country’s approach to content is a reflection of its values........"
            , "web_url": "Test Wrong URL"
            , "pub_date": "2017-05-26T17:51:43+0000"
            , "multimedia": [
                ["subtype" : "wide", "url":"images/2017/05/22/opinion/22jawalSub/22jawalSub-thumbWide-v2.jpg"]
            ]
        ]
        let article = Article.serialize(json as AnyObject)
        XCTAssertNil(article)
    }
    
    func testArticleSerializeNoPublishDate() {
        let json: [String: Any] = [
            "headline": [
                "print_headline": "Singapore’s Media Values"
            ]
            , "snippet": "The Singaporean ambassador writes that his country’s approach to content is a reflection of its values........"
            , "web_url": "https://www.nytimes.com/2017/05/26/opinion/singapores-media-values.html"
            , "multimedia": [
                ["subtype" : "wide", "url":"images/2017/05/22/opinion/22jawalSub/22jawalSub-thumbWide-v2.jpg"]
            ]
        ]
        let article = Article.serialize(json as AnyObject)
        XCTAssertNil(article)
    }
    
    func testArticleSerializeWrongDate() {
        let json: [String: Any] = [
            "headline": [
                "print_headline": "Singapore’s Media Values"
            ]
            , "snippet": "The Singaporean ambassador writes that his country’s approach to content is a reflection of its values........"
            , "web_url": "https://www.nytimes.com/2017/05/26/opinion/singapores-media-values.html"
            , "pub_date": "2017-05-26 17:51:43"
            , "multimedia": [
                ["subtype" : "wide", "url":"images/2017/05/22/opinion/22jawalSub/22jawalSub-thumbWide-v2.jpg"]
            ]
        ]
        let article = Article.serialize(json as AnyObject)
        XCTAssertNil(article)
    }

    func testArticleSerializeNoMultimedia() {
        let json: [String: Any] = [
            "headline": [
                "print_headline": "Singapore’s Media Values"
            ]
            , "snippet": "The Singaporean ambassador writes that his country’s approach to content is a reflection of its values........"
            , "web_url": "https://www.nytimes.com/2017/05/26/opinion/singapores-media-values.html"
            , "pub_date": "2017-05-26T17:51:43+0000"
        ]
        let article = Article.serialize(json as AnyObject)
        XCTAssertNotNil(article)
    }
    
    func testArticleSerializeNoWideImage() {
        let json: [String: Any] = [
            "headline": [
                "print_headline": "Singapore’s Media Values"
            ]
            , "snippet": "The Singaporean ambassador writes that his country’s approach to content is a reflection of its values........"
            , "web_url": "https://www.nytimes.com/2017/05/26/opinion/singapores-media-values.html"
            , "pub_date": "2017-05-26T17:51:43+0000"
            , "multimedia": [
            ]
        ]
        let article = Article.serialize(json as AnyObject)
        XCTAssertNotNil(article)
    }
    
    func testArticleSerializeFull() {
        let json: [String: Any] = [
            "headline": [
                "print_headline": "Singapore’s Media Values"
            ]
            , "snippet": "The Singaporean ambassador writes that his country’s approach to content is a reflection of its values........"
            , "web_url": "https://www.nytimes.com/2017/05/26/opinion/singapores-media-values.html"
            , "pub_date": "2017-05-26T17:51:43+0000"
            , "multimedia": [
                ["subtype" : "wide", "url":"images/2017/05/22/opinion/22jawalSub/22jawalSub-thumbWide-v2.jpg"]
            ]
        ]
        if let article = Article.serialize(json as AnyObject) {
            XCTAssertEqual(["Singapore’s Media Values", "The Singaporean ambassador writes that his country’s approach to content is a reflection of its values........", "https://www.nytimes.com/2017/05/26/opinion/singapores-media-values.html", "images/2017/05/22/opinion/22jawalSub/22jawalSub-thumbWide-v2.jpg"], [article.title, article.snippet, article.web_url.absoluteString, article.image!])
        } else {
            XCTAssertTrue(false, "testArticleSerializeFull failed")
        }
        
    }
}
