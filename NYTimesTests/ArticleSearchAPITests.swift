//
//  ArticleSearchAPITests.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/4/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import XCTest

class ArticleSearchAPITests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testDeserializeRequestPayload() {
        let requestPayload = ArticleSearchAPIRequestPayload(query: "test query", page: 10)
        let parameters = requestPayload.deserialize()
        XCTAssertEqual(parameters["q"] as? String, "test query")
        XCTAssertEqual(parameters["page"] as? Int, 10)
        XCTAssertEqual(parameters["api-key"] as? String, "137d2532a30e43fd972b6ed4098a7e64")
    }
    
    func testSerializeResponsePayloadNotDictionary() {
        let json = "None Dictionary Test"
        let article = ArticleSearchAPIResponsePayload.serialize(json as AnyObject)
        XCTAssertNil(article)
    }
    
    func testArticleSerializeNormal() {
        let json: [String: Any] =
            ["response":
                ["docs": [
                    ["headline": [
                        "print_headline": "Title 1"
                        ]
                        , "snippet": "Snippet 1"
                        , "web_url": "https://www.nytimes.com/2017/05/26/opinion/singapores-media-values.html"
                        , "pub_date": "2017-05-26T17:51:43+0000"
                        , "multimedia": [
                            ["subtype" : "wide", "url":"images/2017/05/22/opinion/22jawalSub/22jawalSub-thumbWide-v2.jpg"]
                            ]
                        ],
                    ["headline": [
                        "print_headline": "Title 2"
                        ]
                        , "snippet": "Snippet 2"
                        , "web_url": "https://www.nytimes.com/2017/05/26/opinion/singapores-media-values.html"
                        , "pub_date": "2017-05-26T17:51:43+0000"
                        , "multimedia": [
                            ["subtype" : "wide", "url":"images/2017/05/22/opinion/22jawalSub/22jawalSub-thumbWide-v2.jpg"]
                            ]
                        ]
                    ]
                ]
            ]
        let response = ArticleSearchAPIResponsePayload.serialize(json as AnyObject)
        XCTAssertEqual(response?.articles.count, 2)
    }
    
}
