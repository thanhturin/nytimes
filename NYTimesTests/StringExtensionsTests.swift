//
//  StringExtensionsTests.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/4/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import XCTest

class StringExtensionsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetTimeDateFromString() {
        let dateString = "2017-05-26T17:51:43+0000"
        let dateTime = dateString.getDateTime()
        XCTAssertNotNil(dateTime)
    }
    
    func testGetTimeStringFromDate() {
        let date = Date()
        let dateString = date.getTimeString()
        XCTAssertEqual(dateString.characters.count, 16)
    }
    
}
