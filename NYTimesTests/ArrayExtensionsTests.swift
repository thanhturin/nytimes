//
//  ArrayExtensionsTests.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/4/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import XCTest

class ArrayExtensionsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testUniqueEmptyArray() {
        let array: [String] = []
        let uniqueArray = array.unique()
        let expectedArray: [String] = []
        XCTAssertEqual(uniqueArray, expectedArray)
    }
    
    func testUniqueOneElementArray() {
        let array: [String] = ["test"]
        let uniqueArray = array.unique()
        let expectedArray: [String] = ["test"]
        XCTAssertEqual(uniqueArray, expectedArray)
    }
    
    func testUniqueRandomArray() {
        let array: [String] = ["1", "2", "3", "1", "3"]
        let uniqueArray = array.unique()
        let expectedArray: [String] = ["1", "2", "3"]
        XCTAssertEqual(uniqueArray, expectedArray)
    }
}
