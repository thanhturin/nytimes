//
//  HomeUITests.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/6/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import XCTest

class HomeUITests: UITests {
        
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testShowEmptyFeedViewAtBegining() {
        XCTAssertTrue(app.otherElements["home_empty_view"].exists)
        XCTAssertTrue(app.buttons["home_empty_search_button"].exists)
    }
    
    func testTapEmptySearchButton() {
        app.buttons["home_empty_search_button"].tap()
        app.keys["S"].tap()
        app.keys["i"].tap()
        app.keys["n"].tap()
        app.keys["g"].tap()
        app.keys["a"].tap()
        app.keys["p"].tap()
        app.keys["o"].tap()
        app.keys["r"].tap()
        app.keys["e"].tap()
        app.typeText("\n")
    }
    
    func testTapSearchBar() {
        app.buttons["home_search_bar"].tap()
        app.keys["S"].tap()
        app.keys["i"].tap()
        app.keys["n"].tap()
        app.keys["g"].tap()
        app.keys["a"].tap()
        app.keys["p"].tap()
        app.keys["o"].tap()
        app.keys["r"].tap()
        app.keys["e"].tap()
        app.typeText("\n")
    }
    
    func testShowRecentSearch() {
        app.buttons["home_search_bar"].tap()
        app.searchFields["Search Articles"].typeText("American")
        app.typeText("\n")
        app.buttons["home_search_bar"].tap()
        XCTAssertTrue(app.tables["search_table_view"].cells.count  == 1)
    }
    
    func testShowTheLatest10RecentSearch() {
        for i in 0...11 {
            app.buttons["home_search_bar"].tap()
            app.searchFields["Search Articles"].typeText("Test \(i)")
            app.typeText("\n")
        }
        
        app.buttons["home_search_bar"].tap()
        XCTAssertTrue(app.tables["search_table_view"].cells.count == 10)
    }
    
    func testShowTapRecentSearch() {
        app.buttons["home_search_bar"].tap()
        app.searchFields["Search Articles"].typeText("American")
        app.typeText("\n")
        app.buttons["home_search_bar"].tap()
        app.tables["search_table_view"].staticTexts["American"].tap()
    }
    
    func testShowingArticle() {
        app.buttons["home_search_bar"].tap()
        app.searchFields["Search Articles"].typeText("American")
        app.typeText("\n")
        
        // wait for network
        sleep(10)
        
        let firstChild = app.collectionViews.children(matching:.any).element(boundBy: 0)
        if firstChild.exists {
            firstChild.tap()
        } else {
            XCTAssert(false, "No Collection Views")
        }
    }
}
