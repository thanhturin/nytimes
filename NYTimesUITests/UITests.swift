//
//  ResetCache.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/7/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import XCTest

class UITests: XCTestCase {
    let app = XCUIApplication()
    
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app.launchArguments += ["UI-Testing"]
        app.launch()
    }
}
