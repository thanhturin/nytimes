//
//  ShareInstances.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/8/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

let networkServiceDefault = NetworkService()

let apiEndPoint = "https://api.nytimes.com"
let imageEndPoint = "https://static01.nyt.com/"

let suggestionSearchServiceDefault = SuggestionSearchServiceDefault(suggestionSearches: SuggestionSearchServiceDefault.loadCacheForRecentSearch(key: AppState.release.suggestionSearchKey))
