//
//  ViewModel.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/3/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

protocol ViewModelBindable {
    associatedtype ViewModelType: ViewModel
    var viewModel: ViewModelType! { get set }
    func bind()
}

class ViewModel: NSObject {
    
}
