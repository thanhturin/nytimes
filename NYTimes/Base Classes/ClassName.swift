//
//  ClassName.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/3/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

protocol ClassName {
    static var className: String { get }
}

// Swift Objects
extension ClassName {
    static var className: String {
        return String(describing: self)
    }
}

extension NSObject: ClassName {}
