//
//  ArticleSearchAPI.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/1/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

struct ArticleSearchAPIRequestPayload {
    let query: String
    let page: Int
    let key: String
    
    init(
        query: String
        , page: Int
        , key: String = "137d2532a30e43fd972b6ed4098a7e64"
        ) {
        self.query = query
        self.page = page
        self.key = key
    }
}

public protocol Deserializable {
    func deserialize() -> [String: AnyObject]
}


extension ArticleSearchAPIRequestPayload: Deserializable {
    func deserialize() -> [String: AnyObject] {
        var parameters = [String: AnyObject]()
        
        parameters["q"] = query as AnyObject?
        parameters["page"] = page as AnyObject?
        parameters["api-key"] = key as AnyObject?
        
        return parameters
    }
}

protocol ArticleSearchAPI {
    func request(withRequestPayload requestPayload: ArticleSearchAPIRequestPayload, completion: @escaping (_ data: ArticleSearchAPIResponsePayload?, _ error: Error?) -> Void)
}


final class ArticleSearchAPIDefault: ArticleSearchAPI {

    private let networkService: NetworkService
    
    init(networkService: NetworkService = networkServiceDefault){
        self.networkService = networkService
    }
    
    func request(withRequestPayload requestPayload: ArticleSearchAPIRequestPayload, completion: @escaping (_ data: ArticleSearchAPIResponsePayload?, _ error: Error?) -> Void) {
        let url = apiEndPoint + "/svc/search/v2/articlesearch.json"
        
        let parameters = requestPayload.deserialize()
        networkService.loadDataFromURL(url: url, parameters: parameters) { (data: Data?, error: Error?) in
            let response = { () -> ArticleSearchAPIResponsePayload? in
                guard let responseData = data else {
                    print("Error: did not receive data")
                    return nil
                }
                
                guard let json = try? JSONSerialization.jsonObject(with: responseData, options: []) else {
                    print("error trying to convert data to JSON")
                    return nil
                }
                
                return ArticleSearchAPIResponsePayload.serialize(json as AnyObject)
            }()
            completion(response, error)
        }
    }
}

struct ArticleSearchAPIResponsePayload {
    // Array
    let articles: [Article]
    
    init(articles: [Article]) {
        self.articles = articles
    }
}

extension ArticleSearchAPIResponsePayload: Serializable {
    static func serialize(_ jsonRepresentation: AnyObject?) -> ArticleSearchAPIResponsePayload? {
        guard let json = jsonRepresentation as? [String : AnyObject] else {
            return nil
        }
        
        // Array
        var articles = [Article]()
        if let items = json["response"]?["docs"] as? [AnyObject] {
            for item in items {
                if let article = Article.serialize(item) {
                    articles.append(article)
                }
            }
        }
        
        let result = ArticleSearchAPIResponsePayload(articles: articles)
        return result
    }
}

