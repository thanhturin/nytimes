//
//  Article.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/1/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

final class Article {
    
    let title: String
    let web_url: URL
    let snippet: String
    let pub_date: Date
    let image: String?
    
    init(
        title: String
        , web_url: URL
        , snippet: String
        , pub_date: Date
        , image: String?
        ) {
        self.title = title
        self.web_url = web_url
        self.snippet = snippet
        self.pub_date = pub_date
        self.image = image
    }
}

public protocol Serializable {
    associatedtype SerializableType
    static func serialize(_ jsonRepresentation: AnyObject?) -> SerializableType?
}

extension Article: Serializable {
    final class func serialize(_ jsonRepresentation: AnyObject?) -> Article? {
        
        guard let json = jsonRepresentation as? [String: AnyObject] else {
            return nil
        }
        
        guard let title = { () -> String? in
            guard let headline = json["headline"] as? [String: AnyObject] else  {
                return nil
            }
            return headline["print_headline"] as? String
            }() else {
                return nil
        }
        
        guard let snippet = json["snippet"] as? String else {
            return nil
        }
        
        guard let web_url = { () -> URL? in
            guard let urlString = json["web_url"] as? String else {
                return nil
            }
            return URL(string: urlString)
            }() else {
                return nil
        }
        
        guard let pub_date = { () -> Date? in
            let value = json["pub_date"] as? String
            return value?.getDateTime()
            }() else {
                return nil
        }
        
        let image = { () -> String? in
            guard let value = json["multimedia"] as? [[String: AnyObject]] else {
                return nil
            }
            
            for item in value {
                if let subtype = item["subtype"] as? String, subtype == "wide" {
                    return item["url"] as? String
                }
            }
            return nil
        }()
        
        let result = Article(
            title: title
            , web_url: web_url
            , snippet: snippet
            , pub_date: pub_date
            , image: image
        )
        return result
    }
}

