//
//  SearchViewController.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/1/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    
    var viewModel: SearchViewControllerViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchBar.becomeFirstResponder()
    }
    
    func configureView() {
        tableView.register(UINib(nibName: SuggestionSearchTableViewCell.className, bundle: nil), forCellReuseIdentifier: SuggestionSearchTableViewCell.className)
        
        tableView.tableFooterView = UIView()
        tableView.contentInset = UIEdgeInsetsMake(0, 0, 20, 0)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame: NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        self.tableViewBottomConstraint.constant = keyboardRectangle.size.height + 0.5
        self.view.layoutIfNeeded()
    }
}


extension SearchViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.suggestionSearches.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let searchQuery = viewModel.suggestionSearches[indexPath.row]
        let cellViewModel = SuggestionSearchTableViewCellViewModel(searchQuery: searchQuery)
        let cell = tableView.dequeueReusableCell(withIdentifier: SuggestionSearchTableViewCell.className, for: indexPath) as! SuggestionSearchTableViewCell
        cell.viewModel = cellViewModel
        return cell
    }
}

extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        updateQuery(viewModel.suggestionSearches[indexPath.row])
    }
    
    func updateQuery(_ query: String) {
        navigationController?.popViewController(animated: true)
        viewModel.searchDidSearch(query: query)
    }
}

extension SearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        updateQuery(searchBar.text ?? "")
    }
}

extension SearchViewController: Refreshable {
    func startLoading() {
        //
    }
    
    func refresh() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

extension SearchViewController: ViewControllerBuildable {
    static func build(_ builder: SearchViewControllerViewModel) -> SearchViewController {
        let storyboard = UIStoryboard(name: "Search", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: SearchViewController.className) as! SearchViewController
        viewController.viewModel = builder
        return viewController
    }
}


