//
//  SuggestionSearchTableViewCell.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/4/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import UIKit

class SuggestionSearchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var searchQueryLabel: UILabel!
    
    var viewModel: SuggestionSearchTableViewCellViewModel! {
        didSet {
            bind()
        }
    }
}

extension SuggestionSearchTableViewCell: ViewModelBindable {
    func bind() {
        searchQueryLabel.text = viewModel.searchQuery
    }
}
