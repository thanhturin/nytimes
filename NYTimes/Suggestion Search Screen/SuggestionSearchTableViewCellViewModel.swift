//
//  SuggestionSearchTableViewViewModel.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/4/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

class SuggestionSearchTableViewCellViewModel: ViewModel {
    let searchQuery: String
    
    init(searchQuery: String) {
        self.searchQuery = searchQuery
        super.init()
    }
}
