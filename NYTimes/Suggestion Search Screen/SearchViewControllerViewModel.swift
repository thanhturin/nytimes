//
//  SearchViewControllerViewModel.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/3/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

protocol SearchQueryProtocol: class {
    func searchUpdateQuery(_ query: String)
}

class SearchViewControllerViewModel: ViewModel {
    
    weak var delegate: SearchQueryProtocol?
    let suggestionSearchService: SuggestionSearchService
    var suggestionSearches: [String]
    
    init(delegate: SearchQueryProtocol?
        , suggestionSearchService: SuggestionSearchService = suggestionSearchServiceDefault
        ) {
        self.delegate = delegate
        self.suggestionSearches = suggestionSearchService.suggestionSearches
        self.suggestionSearchService = suggestionSearchService
        super.init()
    }
    
    func searchDidSearch(query: String) {
        suggestionSearchService.cacheLatestSearchQuery(query)
        delegate?.searchUpdateQuery(query)
    }
}
