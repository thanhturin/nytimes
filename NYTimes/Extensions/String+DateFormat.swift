//
//  String+Extension.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/1/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

private let TimeFormat = "yyyy-MM-dd HH:mm"
private let RFC3339DateFormatFractional = "yyyy-MM-dd'T'HH:mm:ssZ"

private let dateFormatterUnlocalized: DateFormatter = {
    let dateFormatterUnlocalized = DateFormatter()
    let enUSPOSIXLocale = Locale(identifier: "en_US_POSIX")
    dateFormatterUnlocalized.locale = enUSPOSIXLocale
    return dateFormatterUnlocalized
}()

extension String {
    func getDateTime() -> Date? {
        dateFormatterUnlocalized.dateFormat = RFC3339DateFormatFractional
        return dateFormatterUnlocalized.date(from: self)
    }
}

extension Date {
    func getTimeString() -> String {
        dateFormatterUnlocalized.dateFormat = TimeFormat
        let result = dateFormatterUnlocalized.string(from: self)
        return result
    }
}
