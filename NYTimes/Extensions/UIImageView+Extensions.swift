//
//  UIImageView+Extensions.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/1/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    func setImage(url: URL, placeHolder: UIImage?) {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                guard
                    let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                    let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                    let data = data, error == nil,
                    let image = UIImage(data: data)
                    else {
                        self.image = placeHolder
                        return
                }
                
                DispatchQueue.main.async() { () -> Void in
                    self.image = image
                }
                }.resume()
        }
    }
    func setImage(link: String, placeHolder: UIImage?) {
        let urlString = "\(imageEndPoint)\(link)"
        guard let url = URL(string: urlString) else {
            self.image = placeHolder
            return
        }
        
        setImage(url: url, placeHolder: placeHolder)
    }
}
