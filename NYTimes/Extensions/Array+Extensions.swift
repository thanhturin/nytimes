//
//  Array+Extensions.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/4/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

extension Array where Element: Hashable {
    func unique() -> [Element] {
        var array: [Element] = []
        
        for element in self where !array.contains(element) {
            array.append(element)
        }
        
        return array
    }
}
