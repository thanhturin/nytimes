//
//  Dictionary+Extensions.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/1/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

extension Dictionary {
    func stringFromHttpParameters() -> String {
        return self.map({ (key, value) -> String in
            let valueEncoding = String(describing: value).addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
            return "\(key)=\(valueEncoding)"
        }).joined(separator: "&")
    }    
}
