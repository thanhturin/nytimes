//
//  NetworkService.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/1/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

class NetworkService {
    
    public func loadDataFromURL(url: String, parameters: [String: AnyObject], completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        let parameterString = parameters.stringFromHttpParameters()
        
        let requestURL = URL(string:"\(url)?\(parameterString)")!
        var request = URLRequest(url: requestURL)
        request.httpMethod = "GET"
        
        print(request.url?.absoluteString ?? "Empty URL")
        
        let loadDataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let _ = error {
                completion(nil, error)
            } else if let response = response as? HTTPURLResponse {
                if response.statusCode != 200 {
                    let statusError = NSError(domain: "com.thanhturin",
                                              code: response.statusCode,
                                              userInfo: [NSLocalizedDescriptionKey: "HTTP status code has unexpected value."])
                    completion(nil, statusError)
                } else {
                    completion(data, nil)
                }
            }
        }
        loadDataTask.resume()
    }
}
