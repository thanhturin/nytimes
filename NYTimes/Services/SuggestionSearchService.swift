//
//  SuggestionSearchService.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/4/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

enum AppState {
    case release
    case unittest
    
    var suggestionSearchKey: String {
        switch self {
        case .release:
            return "release.SuggestionSearchServiceDefault.suggestionSearches"
        case .unittest:
            return "unittest.SuggestionSearchServiceDefault.suggestionSearches"
        }
    }
}

protocol SuggestionSearchService {
    var suggestionSearches: [String] { get }
    func cacheLatestSearchQuery(_ recentSearch: String)
}

class SuggestionSearchServiceDefault: SuggestionSearchService {
    
    var suggestionSearches: [String]
    let cachingKey: String
    
    init(suggestionSearches: [String]
        , cachingKey: String = AppState.release.suggestionSearchKey) {
        self.cachingKey = cachingKey
        self.suggestionSearches = suggestionSearches
    }
    
    func cacheLatestSearchQuery(_ recentSearch: String) {
        var recentSearchesArray = suggestionSearches
        
        if recentSearchesArray.isEmpty {
            recentSearchesArray.append(recentSearch)
        } else {
            recentSearchesArray.insert(recentSearch, at: 0)
        }
        
        var uniqueArray = recentSearchesArray.unique()
        uniqueArray = Array(uniqueArray[0 ... min(9, uniqueArray.count - 1)])
        
        suggestionSearches = uniqueArray
        SuggestionSearchServiceDefault.cache(uniqueArray, key: cachingKey)
    }
}

extension SuggestionSearchServiceDefault {
    
    fileprivate class func cache(_ recentSearches: [String], key: String) {
        let data = NSKeyedArchiver.archivedData(withRootObject: recentSearches)
        UserDefaults.standard.set(data, forKey: key)
    }
    
    fileprivate class func clearCacheForRecentSearch(key: String) {
        UserDefaults.standard.set(nil, forKey: key)
    }
    
    class func loadCacheForRecentSearch(key: String) -> [String] {
        guard let data = UserDefaults.standard.object(forKey: key) as? Data else {
            return []
        }
        
        guard let recentSearch = NSKeyedUnarchiver.unarchiveObject(with: data) as? [String] else {
            clearCacheForRecentSearch(key: key)
            return []
        }
        
        return recentSearch
    }
}
