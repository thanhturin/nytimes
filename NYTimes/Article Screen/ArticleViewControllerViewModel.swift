//
//  ArticleViewControllerViewModel.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/3/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

class ArticleViewControllerViewModel: ViewModel {
    
    let title: String
    let url: URL
    
    init(title: String, url: URL) {
        self.title = title
        self.url = url
        super.init()
    }
}
