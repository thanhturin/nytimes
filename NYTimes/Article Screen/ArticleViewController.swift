//
//  ArticleViewController.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/3/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import UIKit
import WebKit

class ArticleViewController: UIViewController {    
    var progressView = UIProgressView()
    var webview = WKWebView()
    
    var viewModel: ArticleViewControllerViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureWebView()
        configureProgressBar()
        bind()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        progressView.alpha = 0.0
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        progressView.removeFromSuperview()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        progressView.alpha = 1.0
    }
    
    fileprivate func configureWebView() {
        let wkWebViewConfiguration = WKWebViewConfiguration()
        webview = WKWebView(frame: view.bounds, configuration: wkWebViewConfiguration)
        
        webview.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
        webview.addObserver(self, forKeyPath: "loading", options: .new, context: nil)
        
        view.addSubview(webview)
    }

    fileprivate func configureProgressBar() {
        progressView = UIProgressView(frame: CGRect(x: 0, y: navigationController!.navigationBar.frame.size.height - 2, width: view.frame.size.width, height: 10))
        progressView.progressViewStyle = .bar
        progressView.progressTintColor = UIColor.blue
        progressView.setProgress(0.2, animated: true)
        navigationController?.navigationBar.addSubview(progressView)
    }
    
    deinit {
        self.webview.removeObserver(self, forKeyPath: "estimatedProgress")
        self.webview.removeObserver(self, forKeyPath: "loading")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of _: Any?, change _: [NSKeyValueChangeKey: Any]?, context _: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            progressView.alpha = 1.0
            progressView.setProgress(Float(webview.estimatedProgress), animated: true)
            
            if webview.estimatedProgress >= 1.0 {
                UIView.animate(withDuration: 0.3, delay: 0.1, options: UIViewAnimationOptions(), animations: {
                    self.progressView.alpha = 0.0
                }, completion: { (_: Bool) in
                    self.progressView.progress = 0
                })
            }
        } else if keyPath == "loading" {
            UIApplication.shared.isNetworkActivityIndicatorVisible = webview.isLoading
        }
    }
}

extension ArticleViewController: ViewModelBindable {
    func bind() {
        title = viewModel.title
        webview.load(URLRequest(url: viewModel.url))
    }
}

extension ArticleViewController: ViewControllerBuildable {
    static func build(_ builder: ArticleViewControllerViewModel) -> ArticleViewController {
        let storyboard = UIStoryboard(name: "Article", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: ArticleViewController.className) as! ArticleViewController
        viewController.viewModel = builder
        return viewController
    }
}
