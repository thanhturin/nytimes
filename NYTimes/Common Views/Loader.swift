//
//  Loader.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/8/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation
import UIKit

class Loader {
    
    var container: UIView! = UIView()
    var loadingView: UIView! = UIView()
    var activityIndicator: UIActivityIndicatorView! = UIActivityIndicatorView()
    
    init(containerView: UIView) {
        container.frame = containerView.frame
        container.center = containerView.center
        container.backgroundColor = UIColor(white: 1.0, alpha: 0.3)
        containerView.accessibilityIdentifier = "loader_container_view"
        
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center = containerView.center
        loadingView.backgroundColor = UIColor.lightGray
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        activityIndicator.center = CGPoint(x: loadingView.frame.size.width/2, y:  loadingView.frame.size.height/2)
        
        loadingView.addSubview(activityIndicator)
        container.addSubview(loadingView)
        containerView.addSubview(container)
    }
    
    func show() {
        activityIndicator.startAnimating()
        container.isHidden = false
    }
    
    func hide() {
        activityIndicator.stopAnimating()
        container.isHidden = true
    }
}
