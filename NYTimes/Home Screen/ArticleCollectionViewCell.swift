//
//  ArticleCollectionViewCell.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/2/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import UIKit

final class ArticleCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var articleImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var snippetLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBAction func didTapOnRow(_ sender: Any) {
        viewModel.didTapOnRow()
    }
    
    var viewModel: ArticleCollectionViewCellViewModel! {
        didSet {
            bind()
        }
    }
}

extension ArticleCollectionViewCell: ViewModelBindable {
    func bind() {
        titleLabel.text = viewModel.title
        snippetLabel.text = viewModel.snippet
        articleImageView.isHidden = viewModel.articleImageViewIsHidden
        if let imageURL = viewModel.articleImageLink {
            articleImageView.setImage(link: imageURL, placeHolder: nil)
        }
        dateLabel.text = viewModel.date
    }
}
