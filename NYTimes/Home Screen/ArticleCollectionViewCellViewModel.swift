//
//  Created by Thanh KFit on 6/2/17.
//    Copyright © 2017 Thanh KFit. All rights reserved.
//  See LICENSE.txt for license information
//

import Foundation
import UIKit

class ArticleCollectionViewCellViewModel: ViewModel {
    
    let article: Article
    let articleImageLink: String?
    let articleImageViewIsHidden: Bool
    let title: String
    let snippet: String
    let date: String
    let itemSize: CGSize
    
    init(article: Article) {
        self.article = article
        self.articleImageLink = article.image
        self.articleImageViewIsHidden = article.image == nil
        self.title = article.title
        self.snippet = article.snippet
        self.date = article.pub_date.getTimeString()
        
        self.itemSize = { () -> CGSize in
            let width = UIScreen.main.bounds.width
            
            let topHeight: CGFloat = 10.0
            
            let titleHeight: CGFloat = article.title.heightWithConstrainedWidth(width - 20, font: UIFont.boldSystemFont(ofSize: 18.0)) + 10
            
            let imageHeight: CGFloat = article.image == nil ? 0 : 138.0
            
            let dateHeight: CGFloat = article.pub_date.getTimeString().heightWithConstrainedWidth(width - 20.0, font: UIFont.systemFont(ofSize: 12)) + 10
            
            let snippetHeight: CGFloat = article.snippet.heightWithConstrainedWidth(width - 20.0, font: UIFont.systemFont(ofSize: 16)) + 10
            
            let bottomHeight: CGFloat = 10.0
            
            let height = topHeight + titleHeight + imageHeight + dateHeight + snippetHeight + bottomHeight
            
            return CGSize(width: width, height: height)
        }()

    }
    
    func didTapOnRow() {
        if let currentViewController = UIApplication.topViewController() {
            let vm = ArticleViewControllerViewModel(title: article.title, url: article.web_url)
            let vc = ArticleViewController.build(vm)
            currentViewController.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
