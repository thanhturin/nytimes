//
//  HomeViewController.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/1/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var emptyViewSearchButton: UIButton!
    
    var loader: Loader!
    
    @IBAction func didTapSearchButton(_ sender: Any) {
        let vm = SearchViewControllerViewModel(delegate: self)
        let vc = SearchViewController.build(vm)
        navigationController?.pushViewController(vc, animated: true)
    }
    var viewModel: HomeViewControllerViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loader = Loader(containerView: view)
        viewModel = HomeViewControllerViewModel(delegate: self)
        configureView()
    }
    
    func configureView() {
        collectionView.register(UINib(nibName: ArticleCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: ArticleCollectionViewCell.className)
        collectionView.register(UINib.init(nibName: "ArticleCollectionFooterView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "ArticleCollectionFooterView")
        
        emptyViewSearchButton.layer.borderWidth = 1.0
        emptyViewSearchButton.layer.cornerRadius = 3.0
        emptyViewSearchButton.layer.borderColor = UIColor(red: 50.0/255.0, green: 150.0/255.0, blue: 250.0/255.0, alpha: 1.0).cgColor
    }
}

extension HomeViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.collectionViewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellViewModel = viewModel.collectionViewModels[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ArticleCollectionViewCell.className, for: indexPath) as! ArticleCollectionViewCell
        cell.viewModel = cellViewModel
        return cell
    }
}

extension HomeViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard viewModel.articles.count > 0 else {
            return
        }
        
        if indexPath.row == viewModel.articles.count - 1 {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.loadNextPage), object: nil)
            self.perform(#selector(self.loadNextPage), with: nil, afterDelay: 1)
        }
    }
    
    func loadNextPage() {
        viewModel.loadNextPage()
    }
}

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellViewModel = viewModel.collectionViewModels[indexPath.row]
        return cellViewModel.itemSize
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "ArticleCollectionFooterView", for: indexPath as IndexPath)
        return view
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 50)
    }
}

extension HomeViewController: SearchQueryProtocol {
    func searchUpdateQuery(_ query: String) {
        searchBar.text = query
        viewModel.updateQuery(query)
    }
}

extension HomeViewController: Refreshable {
    func startLoading() {
        loader.show()
    }
    
    func refresh() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
            self.emptyView.isHidden = !self.viewModel.articles.isEmpty
            self.loader.hide()
        }
    }
}


