//
//  HomeViewControllerViewModel.swift
//  NYTimes
//
//  Created by Thanh KFit on 6/3/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

struct HomePageState {
    let query: String
    let page: Int
    
    init(query: String, page: Int) {
        self.query = query
        self.page = page
    }
}

class HomeViewControllerViewModel: ViewModel {
    var articles: [Article] = []
    var collectionViewModels: [ArticleCollectionViewCellViewModel] = []
    let articleSearchAPI: ArticleSearchAPI
    weak var delegate: Refreshable?
    var state: HomePageState = HomePageState(query: "", page: 0)
    
    init(
        delegate: Refreshable?
        , articleSearchAPI: ArticleSearchAPI = ArticleSearchAPIDefault()
        ) {
        self.delegate = delegate
        self.articleSearchAPI = articleSearchAPI
        super.init()
        fetchData()
    }
    
    private func fetchData() {
        if state.page == 0 {
            delegate?.startLoading()
        }
        
        let requestPayload = ArticleSearchAPIRequestPayload(query: state.query, page: state.page)
        
        articleSearchAPI.request(withRequestPayload: requestPayload) {
            [weak self] (response: ArticleSearchAPIResponsePayload?, error: Error?) in
            
            guard let strongSelf = self else {
                return
            }
            
            if let responseArticles = response?.articles {
                if strongSelf.state.page == 0 {
                    strongSelf.articles = responseArticles
                } else {
                    strongSelf.articles += responseArticles
                }
            }
            
            strongSelf.collectionViewModels = strongSelf.articles.map({ (article: Article) -> ArticleCollectionViewCellViewModel in
                return ArticleCollectionViewCellViewModel(article: article)
            })
            
            strongSelf.delegate?.refresh()
        }
    }
    
    func updateQuery(_ query: String) {
        state = HomePageState(query: query, page: 0)
        fetchData()
    }
    
    func loadNextPage() {
        state = HomePageState(query: state.query, page: state.page + 1)
        fetchData()
    }
}
